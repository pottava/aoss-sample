Streamlit アプリケーション
===

# 準備

## 初回セットアップ

### サービスアカウントの作成

```sh
gcloud auth login
gcloud iam service-accounts create sa-aoss --display-name "SA for Assured OSS"
gcloud iam service-accounts keys create key.json --iam-account \
    "sa-aoss@$( gcloud config get-value project ).iam.gserviceaccount.com"
```

### pip

```sh
pip install keyring keyrings.google-artifactregistry-auth
```

### Poetry

```sh
poetry add --group dev keyring keyrings.google-artifactregistry-auth
poetry source add --default aoss https://us-python.pkg.dev/cloud-aoss/cloud-aoss-python/simple
poetry source add --secondary pi https://pypi.org/simple
poetry config http-basic.aoss _json_key_base64 "$( base64 -i key.json )"
```

## 依存の管理

### AOSS パッケージの確認

```sh
gcloud auth activate-service-account --key-file key.json
gcloud artifacts files list --project cloud-aoss \
    --repository cloud-aoss-python --location us
gsutil cp -r gs://cloud-aoss/python/google-cloud-bigquery/3.5.0 .
gsutil -m cp -r gs://cloud-aoss/python .
```

### pip

```sh
export GOOGLE_APPLICATION_CREDENTIALS=key.json
vi requirements-google.txt

pip install --require-hashes -r requirements-google.txt -v --no-deps \
    --index-url https://us-python.pkg.dev/cloud-aoss/cloud-aoss-python/simple
pip install -r requirements-pypi.txt --index-url https://pypi.org/simple -v
```

### Poetry

`google-cloud-bigquery` を導入する例をみてみます。入れようとすると `google-crc32c` が AOSS に存在しないとエラーになるため、そちらは PyPI から導入します。

```sh
poetry add --source pi google-crc32c==1.5.0
poetry add --source aoss google-cloud-bigquery
```

# 起動

### 認証

```sh
gcloud auth application-default login
export GCLOUD_PROJECT=$( gcloud config get-value project )
```

### pip

```sh
streamlit run app.py
```

### Poetry

```sh
poetry run streamlit run app.py
```

# コンテナ化と実行

## リポジトリの作成

```sh
gcloud artifacts repositories create my-apps \
    --repository-format docker --location asia-northeast1 \
    --description="Containerized applications"
```

## Cloud Build を使ったビルド

```sh
gcloud builds submit --tag "asia-northeast1-docker.pkg.dev/${project_id}/my-apps/streamlit" .
```

## ローカルでの起動

```sh
gcloud auth configure-docker asia-northeast1-docker.pkg.dev --quiet
docker run --rm -p 8080:8080 asia-northeast1-docker.pkg.dev/${project_id}/my-apps/streamlit
```
