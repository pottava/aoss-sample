FROM python:3.9.17-slim as builder
ENV PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off
WORKDIR /app
COPY encoded.key /app/
COPY requirements-google.txt /app/
RUN pip install --require-hashes -r requirements-google.txt --index-url https://_json_key_base64:$(cat encoded.key)@us-python.pkg.dev/cloud-aoss/cloud-aoss-python/simple -v --no-deps
COPY requirements-pypi.txt /app/
RUN pip install -r requirements-pypi.txt --index-url https://pypi.org/simple -v

FROM python:3.9.17-slim
ENV PYTHONUNBUFFERED=1 \
    PYTHONIOENCODING="UTF-8"
COPY --from=builder /usr/local/lib/python3.9/site-packages /usr/local/lib/python3.9/site-packages
COPY --from=builder /usr/local/bin/streamlit /usr/local/bin/
WORKDIR /app
COPY . /app/
CMD ["streamlit", "run", "app.py", "--server.port", "8080"]
